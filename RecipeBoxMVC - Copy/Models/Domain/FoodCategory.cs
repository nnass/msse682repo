﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain
{
    [Serializable]
    public class FoodCategory
    {
        private string name;

        public FoodCategory() { }

        public void setName(string name) { this.name = name; }
        public string getName() { return name; }
    }

}
