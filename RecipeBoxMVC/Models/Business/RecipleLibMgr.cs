﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Service;
using Domain;

namespace Business
{
    class RecipleLibMgr : Manager
    {
    
        public RecipeLibrary getRecipeLibrary()
        {
            IRecipeLibSvc recipeLibSvc = (IRecipeLibSvc)GetService(typeof(IRecipeLibSvc).Name);
            return recipeLibSvc.GetRecipeLib();
        }
        public void StoreRecipeLibrary(RecipeLibrary recipeLib)
        {
            IRecipeLibSvc recipeLibSvc = (IRecipeLibSvc)GetService(typeof(IRecipeLibSvc).Name);
            recipeLibSvc.StoreRecipeLib(recipeLib);
        }
    }
}
