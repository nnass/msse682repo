﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace Domain
{
    public class EFRecipeRepository : IRecipeRepository
    {
        private RecipeDBContext context = new RecipeDBContext();

        public IQueryable<Recipe> Recipes
        {
            get { return context.Recipes; }
        }
    }
}