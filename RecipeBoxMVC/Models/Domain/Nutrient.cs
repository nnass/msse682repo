﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain
{
    [Serializable]
    public class Nutrient
    {
        public int ID { get; set;}
        public string name { get; set; }
        public double per100Grams { get; set; }

        public Nutrient() { }

        public void setConv(double conv) { this.per100Grams = conv; }
        public double getConv() { return per100Grams; }
    }
}
