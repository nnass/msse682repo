﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;

namespace Domain
{
    [Serializable]
    public class Recipe
    {
        public int ID { get; set; }
        public string name { get; set; }
        public int baseServings { get; set; }
        public int servings { get; set; }
        public string method { get; set; }
        public double timeInMinutes { get; set; }
        public string ownerID { get; set; }
        public List<Ingredient> ingredients { get; set; }
        public List<FoodCategory> categoryTags { get; set; }

        public Recipe() {
      
        }
        public int multServings(int num)
        {
            servings = num * baseServings;
            return servings;
        }

        public int getNumIngred() { return ingredients.Count; }

        public bool validate()
        {
            if (name == null) return false;
            if (baseServings == 0) return false;
            if (method == null) return false;
            if (timeInMinutes == 0) return false;
            if (ingredients == null) return false;
            if (categoryTags == null) return false;
            else return true;
        }


        public override string ToString()
        {
            string ingredientList = "";

            foreach (Ingredient i in ingredients)
            {
                ingredientList = Convert.ToString(i.measurement.amount) + " " +
                                 i.measurement.type + " " + i.name + "\n" + ingredientList;
            }
            return name + "      " + " Time:  " + Convert.ToString(timeInMinutes) + " Minutes" + "\n \n" +
                   "Servings:  " + baseServings + " \n \n" +
                   "Ingredients:  " + ingredientList + "\n \n" +
                   "Directions:  " + method;
                   
                    
        }
    }

    public class RecipeDBContext : DbContext
    {
        public DbSet<Recipe> Recipes { get; set; }
        public DbSet<Ingredient> Ingredients { get; set; }
        public DbSet<FoodCategory> FoodCategories{ get; set; }
        public DbSet<Measurement> Measurements { get; set; }

        public DbSet<Nutrient> Nutrients { get; set; }
    }
}
