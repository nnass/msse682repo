﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain
{
    [Serializable]
    public class FoodCategory
    {
        public int ID { get; set; }
        public string name { get; set; }

        public FoodCategory() { }

    }

}
