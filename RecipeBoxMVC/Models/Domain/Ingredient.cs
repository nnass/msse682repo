﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain
{
    [Serializable]
    public class Ingredient
    {
        public int ID { get; set; }       
        public string name { get; set; }
        public List<Nutrient> nutrients = new List<Nutrient>();
        public Measurement measurement { get; set; }


        public Ingredient() { }

        public List<Nutrient> getNurtients() { return nutrients; }
        public void addNutrient(Nutrient nut) { nutrients.Add(nut); }

        public Measurement convMeasurement(string conversion)
        { //not yet implemented
            return measurement;
        }
    }
}
