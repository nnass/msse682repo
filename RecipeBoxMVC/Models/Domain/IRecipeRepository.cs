﻿using System;
using System.Linq;
using Domain;

namespace Domain
{
    public interface IRecipeRepository
    {
        IQueryable<Recipe> Recipes { get; }
    }
}