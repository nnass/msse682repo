namespace RecipeBoxMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatemodel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Measurements", "type", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Measurements", "type");
        }
    }
}
