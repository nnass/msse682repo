namespace RecipeBoxMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDbforNutrient : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Nutrients",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        name = c.String(),
                        per100Grams = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Nutrients");
        }
    }
}
