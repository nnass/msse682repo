﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Ninject;
using Domain;
using System.Linq;
using System.Collections.Generic;
using RecipeBoxMVC.Controllers;
using System.Web.Mvc;
using System.Web;

namespace RecipeBox.UnitTests
{
    [TestClass]
    public class RecipeTest
    {
        [TestMethod]
        public void Test_Recipe_Owner_Display()
        {
                // Arrange
                // - create the mock repository
                Mock<IRecipeRepository> mock = new Mock<IRecipeRepository>();
                mock.Setup(m => m.Recipes).Returns(new List<Recipe> {
                    new Recipe { name = "Ramen", method = "Boil", servings = 1, timeInMinutes = 5 , ID = 1, ownerID = "Nathan" },
                    new Recipe  { name = "Pad Thai", method = "Pan Fry", servings = 5, timeInMinutes = 20, ID = 2, ownerID = "Nathan" },
                    new Recipe  { name = "Carrot", method = "Slice", servings = 3, timeInMinutes = 4, ID = 2, ownerID = "Bob"}

            }.AsQueryable());

                RecipesController controller = new RecipesController(mock.Object);
                var controllerContext = new Mock<ControllerContext>();

                controllerContext.SetupGet(x =>
                    x.HttpContext.User.Identity.Name).Returns("Nathan");

                controllerContext.SetupGet(x =>
                    x.HttpContext.User.Identity.IsAuthenticated)
                    .Returns(true);

                controllerContext.SetupGet(x =>
                    x.HttpContext.Request.IsAuthenticated)
                    .Returns(true);

                controller.ControllerContext = controllerContext.Object;

                var context =
                    new Mock<HttpContextBase>(MockBehavior.Strict);


                // Action
                IEnumerable<Recipe> result = (IEnumerable<Recipe>)controller.Index("").Model;

                // Assert
                Recipe[] prodArray = result.ToArray();
                Assert.IsTrue(prodArray.Length == 2);
                Assert.AreEqual(prodArray[0].name, "Ramen");
                Assert.AreEqual(prodArray[1].name, "Pad Thai");
        }
    }
}
